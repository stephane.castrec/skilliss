import * as issServices from './iss.services';
import { HandlerInput } from 'ask-sdk';
import { getAddress, getNoAddress } from './geocoder.services';
import * as template from '../skill/template';

export const getIssPosition = async (input: HandlerInput) => {
    const issLocation = await issServices.getIssLocation();
    console.log("iss location", issLocation);
    const address = await getAddress(issLocation.iss_position.latitude, issLocation.iss_position.longitude);
    console.log('location', address.results[0].locations[0]);
    // chec if country available
    if (!address.results[0].locations[0].adminArea1) {
        const city = address.results[0].locations[0].adminArea5;
        const country = address.results[0].locations[0].adminArea1;
        const mapURL = address.results[0].locations[0].mapUrl;
        template.issLocation(input, country, city, mapURL);
    } else {
        const res = await getNoAddress(issLocation.iss_position.latitude, issLocation.iss_position.longitude);
        template.issLocation(input, res.results[0].formatted, issLocation.iss_position.latitude, issLocation.iss_position.longitude);
    }
}  

export const getNbHuman = async (input: HandlerInput) => {
    const humans = await issServices.getHumans();
    console.log("humans", humans);
    
    template.nbHuman(input, humans);
}  
