import * as request from 'request-promise-native';

const url = 'http://www.mapquestapi.com/geocoding/v1/reverse?';
const key = process.env.GEOCODER_API_KEY; 
const openCageKey = process.env.OPENCAGE_API_KEY;
var openCageUrl = 'https://api.opencagedata.com/geocode/v1/json'
export const getAddress = async (lat: number, long: number): Promise<any> => {
    console.log(`lat/long - ${lat}/${long}`);
    var options = {
        uri:`${url}key=${key}&location=${lat},${long}`,
        json: true
    };

    return await request.get(options);
}

export const getNoAddress = async (lat: number, long: number): Promise<any> => {
    console.log(`lat/long - ${lat}/${long}`);
    const u =    openCageUrl + '?'
    + 'key=' + openCageKey
    + '&q=' + encodeURIComponent(lat + ',' + long)
    + '&pretty=1'
    + '&no_annotations=1';
    var options = {
        uri: u,
        json: true
    };

    return await request.get(options);
}