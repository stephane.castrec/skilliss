import * as request from 'request-promise-native';

const baseUrl = 'http://api.open-notify.org';

const doRequest = async (url: string): Promise<any> => {
    var options = {
        uri:url,
        json: true
    };

    return  await request.get(options);
}

export const getHumans = async (): Promise<any> => {
    return doRequest(baseUrl + "/astros.json");
}

export const getIssLocation = async (): Promise<any> => {
    return doRequest(baseUrl + "/iss-now.json");
}
