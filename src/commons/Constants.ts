import { ui } from 'ask-sdk-model';
import { getOne } from './utils/utils';

const messages = {
    'fr-FR': {
        welcome: () => {
            return 'bienvenue sur la station <say-as interpret-as="spell-out">ISS</say-as>. Que souhaitez vous savoir? La position de la station spatiale? Sa vitesse?';
        },
        not_understood: () => {
            return 'Je n\'ai pas compris ce que vous avez dis. Que souhaitez vous?'
        },
        speed: () => {
            return 'La station spatiale se déplace à une vitesse de 7,66 kilomètres par secondes. Souhaitez vous connaitre autre chose? Sa position? Sa date de lancement?';
        },
        dateLaunch: () => {
            return 'La construction de la station spatiale débute avec le lancement le 20 novembre 1998 d\'une fusée Russe metant en orbite le module Zarya. Souhaitez vous une autre information? Sa vitesse? Son orbite?'
        },
        orbite: () => {
            return 'La station spatiale orbite à une hauteur de 408 kilomètres. Que souhaitez vous savoir? La position de la station spatiale ? Son coût ?';
        },
        cost: () => {
            return 'La station spatiale a coûter 150 milliards de dollars. Que souhaitez vous savoir? Combien de personnes se trouvent dans la station ? Sa vitesse?';
        },
        repromptFunctions: () => {
            return 'Que souhaitez vous? La liste des personnes à bord de l\'ISS, ou sa position actuelle?';
        },
        position: (params) => {
            const relance = "Si vous souhaitez connaitre les astronautes à bord, demandez 'qui est dans la station'.";
            if (!params.city)
                return `La station spatiale internationale se trouve actuellement au dessus de ${params.country}. ${relance}`;
            return `La station spatiale internationale se trouve actuellement au dessus de ${params.city}, ${params.country}. ${relance}`;
        },
        human: (params) => {
            let msg = 'Il y a actuellement ' + params.people.length + ' personnes dans la station spatiale. Voici la liste: ';

            for (let i = 0; i < params.people.length; i++) {
                if (i != 0) {
                    msg += ', ';
                }
                msg += params.people[i];
            }
            msg += '. Si vous souhaitez connaitre sa position, dites `sa position`.';
            return msg;
        },
        error: () => {
            return 'Il semblerait que la skill <say-as interpret-as="spell-out">ISS</say-as> ait un problème. Ré-essais dans un instant.';
        },
        help: (): string => {
            return "Avec <say-as interpret-as=\"spell-out\">ISS</say-as>, suivez nos astronautes dans l'espace. Dites simplements, `Alexa, demande à I S S sa position actuelle` ou `Alexa, demande à I S S qui est dans l'espace. Que souhaitez vous savoir? La position de la station spatiale? Sa vitesse?'";
        },
        bye: () => {
            return "A bientôt sur l'<say-as interpret-as=\"spell-out\">ISS</say-as>";
        }
    },
    'default': {
        welcome: () => {
            return 'Welcome  on the <say-as interpret-as="spell-out">ISS</say-as>. What do you want to know? The <say-as interpret-as="spell-out">ISS</say-as> location ? Who\'s in? ';
        },
        not_understood: () => {
            return 'I don\'t inderstand. Can you repeat?'
        },
        speed: () => {
            return 'The space station travels at a speed of 7.66 kilometers per second. Do you want to know something else? Orbit ? Cost ?';
        },
        dateLaunch: () => {
            return 'The construction of the space station begins with the launch on November 20, 1998 of a Russian rocket in orbit module Zarya. Do you want to know something else? Speed? Current location?'
        },
        orbite: () => {
            return 'The space station orbits at a height of 408 kilometers. Do you want to know something else? Speed? Current location? ';
        },
        cost: () => {
            return 'The space station has cost 150 billion dollars. Do you want to know something else? Speed? Current location? ';
        },
        repromptFunctions: () => {
            return 'What do you want to know? The list of people aboard the ISS, or its current position?';
        },
        position: (params) => {
            const relance = "If you want to know who's in, say 'Who's in the space station'.";
            if (!params.city)
                return `the <say-as interpret-as="spell-out">ISS</say-as> is currently above ${params.country}. ${relance}`;
            return `the <say-as interpret-as="spell-out">ISS</say-as> is currently above ${params.city}, ${params.country}. ${relance}`;
        },
        human: (params) => {
            let msg = 'There are currently ' + params.people.length + ' in the space station. Here the astronauts list: ';

            for (let i = 0; i < params.people.length; i++) {
                if (i != 0) {
                    msg += ', ';
                }
                msg += params.people[i];
            }
            msg += '. Do you want to know where they are? Say `Location`.';
            return msg;
        },
        error: () => {
            return 'Looks like <say-as interpret-as="spell-out">ISS</say-as>  skill has trouble. Please try again.';
        },
        help: (): string => {
            return 'With <say-as interpret-as="spell-out">ISS</say-as>, follow our astronauts in space. Just say, `Alexa, ask I S S his location` or `Alexa, ask à <say-as interpret-as="spell-out">ISS</say-as> who is in space. So, what do you want to know about <say-as interpret-as="spell-out">ISS</say-as>? Location? Orbit? Speed?';
        },
        bye: () => {
            return 'See you soon on <say-as interpret-as="spell-out">ISS</say-as>';
        }
    }
}

export const getMessage = (local, key, params?) => {
    let msg;
    console.log(`getMessage for ${local} ${key}`);
    if (messages[local]) {
        msg = messages[local][key](params);
    } else {
        // all english
        msg = messages['default'][key](params);
    }
    return `<speak>${msg}</speak>`;
}