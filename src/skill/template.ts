import { Directive } from "ask-sdk-model";
import aplDoc from "./apls/apl";
import { HandlerInput } from "ask-sdk";
import { getMessage } from "../commons/Constants";
import { SkillInputUtils } from "./SkillInputUtils";

const getAplParam = (imgUrl) => {
    return {
        "bodyTemplate7Data": {
            "type": "object",
            "objectId": "bt7Sample",
            "title": "ISS",
            "image": {
                "contentDescription": null,
                "smallSourceUrl": null,
                "largeSourceUrl": null,
                "sources": [
                    {
                        "url": imgUrl,
                        "size": "small",
                        "widthPixels": 0,
                        "heightPixels": 0
                    },
                    {
                        "url": imgUrl,
                        "size": "large",
                        "widthPixels": 0,
                        "heightPixels": 0
                    }
                ]
            },
            "logoUrl": "https://skilliss.s3.eu-west-3.amazonaws.com/icon_108_A2Z.png",
        }
    }
}

const apl = (mapUrl: string): Directive => {
    return {
        type: 'Alexa.Presentation.APL.RenderDocument',
        document: aplDoc,
        datasources: getAplParam(mapUrl)
    };
}

export const issLocation = (input: HandlerInput, country: string, lat: number, longi: number, city?: string, mapUrl?: string) => {
    const local = new SkillInputUtils(input).language();
    input.responseBuilder.speak(getMessage(local, 'position', { country, city }))
        .reprompt(getMessage(local, 'repromptFunctions'))
        .withShouldEndSession(false);
    if (new SkillInputUtils(input).hasApl()) {
        if (!mapUrl) {
            mapUrl = `https://static-maps.yandex.ru/1.x/?lang=fr_FR&ll=${longi},${lat}&size=450,450&l=map&z=2&pt=${longi},${lat},pm2rdm`
        }
        input.responseBuilder.addDirective(apl(mapUrl));
    }
}

export const nbHuman = (input: HandlerInput, humansList) => {
    const local = new SkillInputUtils(input).language();
    const list = humansList.people.filter(f => f.craft === 'ISS').map(s => s.name);
    input.responseBuilder.speak(getMessage(local, 'human', { people: list }))
        .reprompt(getMessage(local, 'repromptFunctions'))
        .withShouldEndSession(false);

}

