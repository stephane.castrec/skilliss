
'use strict';

import { HandlerInput } from 'ask-sdk';
import { Response } from 'ask-sdk-model';

import { IHandler } from '../commons/utils/IHandler';
import { getMessage } from '../commons/Constants';
import { getNbHuman, getIssPosition } from '../commons/Business';
import { SkillInputUtils } from './SkillInputUtils';

export const IntentHandler: IHandler = {
    // launch request and play intent have the same handler
    'LaunchRequest': async function (input: HandlerInput): Promise<Response> {
        const local = new SkillInputUtils(input).language();
        input.responseBuilder.speak(getMessage(local, 'welcome'))
            .reprompt(getMessage(local, 'repromptFunctions'));
        return input.responseBuilder.getResponse();

    },
    'SpeedIntent': async function (input: HandlerInput): Promise<Response> {
        const local = new SkillInputUtils(input).language();

        input.responseBuilder
            .speak(getMessage(local, 'speed'))
            .reprompt(getMessage(local, 'repromptFunctions'));
        return input.responseBuilder.getResponse();
    },
    'orbiteIntent': async function (input: HandlerInput): Promise<Response> {
        const local = new SkillInputUtils(input).language();

        input.responseBuilder
            .speak(getMessage(local, 'orbite'))
            .reprompt(getMessage(local, 'repromptFunctions'));
        return input.responseBuilder.getResponse();
    },
    'DateLaunchIntent': async function (input: HandlerInput): Promise<Response> {
        const local = new SkillInputUtils(input).language();

        input.responseBuilder
            .speak(getMessage(local, 'dateLaunch'))
            .reprompt(getMessage(local, 'repromptFunctions'));
        return input.responseBuilder.getResponse();
    },
    'costIntent': async function (input: HandlerInput): Promise<Response> {
        const local = new SkillInputUtils(input).language();

        input.responseBuilder
            .speak(getMessage(local, 'cost'))
            .reprompt(getMessage(local, 'repromptFunctions'));
        return input.responseBuilder.getResponse();
    },
    'HumansIntent': async function (input: HandlerInput): Promise<Response> {
        await getNbHuman(input);
        return input.responseBuilder.getResponse();

    },
    'ISSLocationIntent': async function (input: HandlerInput): Promise<Response> {
        await getIssPosition(input);
        return input.responseBuilder.getResponse();

    },
    'AMAZON.NoIntent': async function (input: HandlerInput): Promise<Response> {
        const local = new SkillInputUtils(input).language();
        return input.responseBuilder
            .speak(getMessage(local, 'bye'))
            .withShouldEndSession(true)
            .getResponse();
    },
    'AMAZON.CancelIntent': async function (input: HandlerInput): Promise<Response> {
        const local = new SkillInputUtils(input).language();
        return input.responseBuilder
            .speak(getMessage(local, 'bye'))
            .withShouldEndSession(true)
            .getResponse();
    },
    'AMAZON.StopIntent': async function (input: HandlerInput): Promise<Response> {
        const local = new SkillInputUtils(input).language();

        return input.responseBuilder
            .speak(getMessage(local, 'bye'))
            .withShouldEndSession(true)
            .getResponse();
    },
    'AMAZON.Fallback': async function (input: HandlerInput): Promise<Response> {
        const local = new SkillInputUtils(input).language();

        return input.responseBuilder
            .speak(getMessage(local, 'not_understood'))
            .reprompt(getMessage(local, 'repromptFunctions'))
            .withShouldEndSession(false)
            .getResponse();
    },
    'AMAZON.HelpIntent': async function (input: HandlerInput): Promise<Response> {
        const local = new SkillInputUtils(input).language();

        return input.responseBuilder
            .speak(getMessage(local, 'help'))
            .reprompt(getMessage(local, 'repromptFunctions'))
            .withShouldEndSession(false)
            .getResponse();
    },
    'SessionEndedRequest': async function (input: HandlerInput): Promise<Response> {
        // No session ended logic
        // do not return a response, as per https://developer.amazon.com/docs/custom-skills/handle-requests-sent-by-alexa.html#sessionendedrequest
        return Promise.resolve(input.responseBuilder.getResponse());
    },
    'System.ExceptionEncountered': async function (input: HandlerInput): Promise<Response> {
        const local = new SkillInputUtils(input).language();
        console.log("\n******************* EXCEPTION **********************");
        console.log("\n" + JSON.stringify(input.requestEnvelope, null, 2));
        return input.responseBuilder
            .speak(getMessage(local, 'error'))
            .withShouldEndSession(false)
            .getResponse();
    },
    'Unhandled': async function (input: HandlerInput): Promise<Response> {
        return this['System.ExceptionEncountered'];
    }
}