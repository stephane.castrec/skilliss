'use strict';

import 'mocha';
import { assert } from 'chai';
import * as business from '../src/commons/Business';
import { HandlerInput } from 'ask-sdk';
import { Directive } from 'ask-sdk-model';

describe('Business tests', () => {
    it('business human', async () => {
        const input: HandlerInput = {
            requestEnvelope: {
                "version": "1.0",
                context: null,
                request: {
                    "type": "LaunchRequest",
                    "requestId": "amzn1.echo-api.request.3eb246fe-168a-49aa-b162-e99bb26d7ce3",
                    "timestamp": "2019-11-07T08:09:45Z",
                    "locale": "en-US",
                }
            },
            attributesManager: null,
            responseBuilder: {
                speak: (msg: string) => { return input.responseBuilder},
                addDirective: (directive: Directive) => { return input.responseBuilder},
                reprompt: (msg: string) => { return input.responseBuilder},
                withShouldEndSession: (bo: Boolean) => { return input.responseBuilder},
                withAskForPermissionsConsentCard: () => { return input.responseBuilder},
                withCanFulfillIntent: () => { return input.responseBuilder},
                withLinkAccountCard:() => { return input.responseBuilder},
                withSimpleCard:() => { return input.responseBuilder},
                withStandardCard:() => { return input.responseBuilder},
                addAudioPlayerClearQueueDirective:() => { return input.responseBuilder},
                addAudioPlayerPlayDirective:() => { return input.responseBuilder},
                addAudioPlayerStopDirective:() => { return input.responseBuilder},
                addConfirmIntentDirective:() => { return input.responseBuilder},
                addConfirmSlotDirective:() => { return input.responseBuilder},
                addDelegateDirective:() => { return input.responseBuilder},
                addElicitSlotDirective:() => { return input.responseBuilder},
                addHintDirective:() => { return input.responseBuilder},
                addRenderTemplateDirective:() => { return input.responseBuilder},
                addVideoAppLaunchDirective:() => { return input.responseBuilder},
                getResponse:() => { return {}},
            },
        }
        await business.getIssPosition(input);

    });

    it('business location', async () => {
        const input: HandlerInput = {
            attributesManager: null,
            requestEnvelope: {
                "version": "1.0",
                context: null,
                request: {
                    "type": "LaunchRequest",
                    "requestId": "amzn1.echo-api.request.3eb246fe-168a-49aa-b162-e99bb26d7ce3",
                    "timestamp": "2019-11-07T08:09:45Z",
                    "locale": "en-US",
                }
            },
            responseBuilder: {
                speak: (msg: string) => { return input.responseBuilder},
                addDirective: (directive: Directive) => { return input.responseBuilder},
                reprompt: (msg: string) => { return input.responseBuilder},
                withShouldEndSession: (bo: Boolean) => { return input.responseBuilder},
                withAskForPermissionsConsentCard: () => { return input.responseBuilder},
                withCanFulfillIntent: () => { return input.responseBuilder},
                withLinkAccountCard:() => { return input.responseBuilder},
                withSimpleCard:() => { return input.responseBuilder},
                withStandardCard:() => { return input.responseBuilder},
                addAudioPlayerClearQueueDirective:() => { return input.responseBuilder},
                addAudioPlayerPlayDirective:() => { return input.responseBuilder},
                addAudioPlayerStopDirective:() => { return input.responseBuilder},
                addConfirmIntentDirective:() => { return input.responseBuilder},
                addConfirmSlotDirective:() => { return input.responseBuilder},
                addDelegateDirective:() => { return input.responseBuilder},
                addElicitSlotDirective:() => { return input.responseBuilder},
                addHintDirective:() => { return input.responseBuilder},
                addRenderTemplateDirective:() => { return input.responseBuilder},
                addVideoAppLaunchDirective:() => { return input.responseBuilder},
                getResponse:() => { return {}},
            },
        }
        await business.getNbHuman(input);
        
    });
});