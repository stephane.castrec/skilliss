'use strict';

import 'mocha';
import { assert } from 'chai';
import { getAddress } from '../src/commons/geocoder.services';

describe('Geocoder Service tests', () => {
    it('reverse geocoding', async () => {
        const addresses = await getAddress(30.333472, -81.470448);
        assert.isNotNull(addresses);
        console.log(addresses);
        assert.equal("12714 Ashley Melisse Blvd", addresses.results[0].locations[0].street);
    });
});