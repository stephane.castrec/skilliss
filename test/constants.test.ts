'use strict';

import 'mocha';
import { assert } from 'chai';
import { getMessage } from '../src/commons/Constants';

describe('Get Message', () => {
    it('get US message without param', () => {
        const expected = '<speak>Welcome  on the <say-as interpret-as="spell-out">ISS</say-as>. What do you want to know? The <say-as interpret-as="spell-out">ISS</say-as> location ? Who\'s in? </speak>';
        assert.equal(expected, getMessage('us_US', 'welcome'));
    })

    it('get default message without param', () => {
        assert.equal('<speak>Welcome  on the <say-as interpret-as="spell-out">ISS</say-as>. What do you want to know? The <say-as interpret-as="spell-out">ISS</say-as> location ? Who\'s in? </speak>', getMessage('dummy', 'welcome'));
    })

    it('get FR message with param', () => {
        const expected = '<speak>La station spatiale internationale se trouve actuellement au dessus de France. Si vous souhaitez connaitre les astronautes à bord, demandez \'qui est dans la station\'.</speak>';
        assert.equal(expected, getMessage('fr-FR', 'position', {city: null, country: 'France'}));
    })

    it('get US message with param', () => {
        const expected = '<speak>the <say-as interpret-as="spell-out">ISS</say-as> is currently above France. If you want to know who\'s in, say \'Who\'s in the space station\'.</speak>';
        assert.equal(expected, getMessage('us_US', 'position', {city: null, country: 'France'}));
    })

    it('get default message with param', () => {
        const expected = '<speak>the <say-as interpret-as="spell-out">ISS</say-as> is currently above France. If you want to know who\'s in, say \'Who\'s in the space station\'.</speak>';
        assert.equal(expected, getMessage('dummy', 'position', {city: null, country: 'France'}));
    })
});