'use strict';

import 'mocha';
import { assert } from 'chai';
import { getHumans, getIssLocation } from '../src/commons/iss.services';

describe('ISS Service tests', () => {
    it('getHumans', async () => {
        const people = await getHumans();
        assert.isNotNull(people);
        assert.isNotNull(people.people);
        assert.isTrue(people.people.length > 0);

    });

    it('get iss location', async () => {
        const location = await getIssLocation();
        assert.isNotNull(location);
        assert.isNotNull(location.iss_position);
    });
});